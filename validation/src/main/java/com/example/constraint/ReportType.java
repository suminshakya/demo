package com.example.constraint;

import java.util.Arrays;

public enum ReportType {
    EXCEL("excel"),
    PDF("pdf"),
    WORD("word");

    private final String reportType;

    ReportType(String reportType) {
        this.reportType = reportType;
    }

    public static ReportType validate(String reportType) {
        System.out.println("linked");
        return Arrays.stream(ReportType.values()).filter(r -> r.reportType.equals(reportType)).findFirst()
                .orElseThrow(() -> new RuntimeException("not a record type"));

    }
}
