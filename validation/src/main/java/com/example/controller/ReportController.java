package com.example.controller;

import com.example.constraint.ReportType;
import com.example.service.impl.ReportFactory;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class ReportController {

    private final ReportFactory reportFactory;

    @GetMapping("/report")
    public String getReport(@RequestParam String reportType) {

        ReportType record = ReportType.validate(reportType);
        return reportFactory.getReportService(record).execute();
    }

}

