package com.example.service.impl;

import com.example.constraint.ReportType;
import com.example.service.ReportService;
import org.springframework.stereotype.Service;

@Service
public class PdfServiceImpl implements ReportService {

    @Override
    public String execute() {
        return "This is a pdf service";
    }

    @Override
    public ReportType formatType() {
        return ReportType.PDF;
    }
}
