package com.example.service.impl;

import com.example.constraint.ReportType;
import com.example.service.ReportService;
import org.springframework.stereotype.Service;

@Service
public class WordServiceImpl implements ReportService {

    @Override
    public String execute() {
        return "This is a word service";
    }

    @Override
    public ReportType formatType() {
        return ReportType.WORD;
    }
}
