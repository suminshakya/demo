package com.example.service.impl;

import com.example.constraint.ReportType;
import com.example.service.ReportService;
import org.springframework.stereotype.Service;

@Service
public class ExcelServiceImpl implements ReportService {

    @Override
    public String execute() {
        return "This is excel service";
    }

    @Override
    public ReportType formatType() {
        return ReportType.EXCEL;
    }
}
