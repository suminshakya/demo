package com.example.service.impl;

import com.example.constraint.ReportType;
import com.example.service.ReportService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ReportFactory {

    Map<ReportType, ReportService> reportServiceMap = new HashMap<>();

    public ReportFactory(List<ReportService> reportService) {
        reportService.forEach(report -> reportServiceMap.put(report.formatType(), report));
    }


    public ReportService getReportService(ReportType reportType) {
        return reportServiceMap.get(reportType);
    }
}
