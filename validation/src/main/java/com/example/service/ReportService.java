package com.example.service;

import com.example.constraint.ReportType;

public interface ReportService {

    String execute();

    ReportType formatType();
}
